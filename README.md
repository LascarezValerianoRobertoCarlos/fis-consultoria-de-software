# CONSULTORIA-MAPA CONCEPTUAL
```plantuml
@startmindmap

*[#LightGoldenRodYellow]  **CONSULTORIA**
 *[#LightGreen]  **Definiciòn**
  *[#LightPink]  **Profesiòn de** \n **futuro**
   *_ Ejemplo 
    *[#LightBlue]  **AXPE Consulting**
     *[#LightBlue]  **Compañia multinacional**
   *[#LightBlue]  **Empresa de** \n **servicios** 
    *[#LightBlue]  **Profesionales**
     *_ De alto
      *[#LightBlue]  **Valor añadido**
       *_ Con
        *[#LightBlue]  **Conocimiento**
         *[#LightBlue]  **Sectorial**
         *[#LightBlue]  **Tècnico**
       *_ Con
        *[#LightBlue]  **Capacidad**
         *_ De plantar
          *[#LightBlue]  **Soluciones**
 *[#LightGreen]  **Tipos de servicios**
  *[#LightPink]  **ÀMBITOS**
   *[#LightYellow]  **Servicios**
    *[#LightGreen]  **Consultorìa**
     *[#LightBlue]  **Ingenierìa de procesos**
     *[#LightBlue]  **Gobierno TI**
     *[#LightBlue]  **Oficina de proyectos**
     *[#LightBlue]  **Analìtica**
      *_ avanzada de
       *[#LightBlue]  **Datos**
    *[#LightGreen]  **Integraciòn**
     *[#LightBlue]  **Desarrollos a medida**
     *[#LightBlue]  **Aseguramiento**
      *_ de la 
       *[#LightBlue]  **Calidad**
     *[#LightBlue]  **Infraestructuras**
     *[#LightBlue]  **Soluciones de mercado**
    *[#LightGreen]  **Externalizaciòn**
     *[#LightBlue]  **Gestiòn de aplicaciones**
     *[#LightBlue]  **Servicios SQA**
     *[#LightBlue]  **Operacion y administraciòn**
      *_ de 
       *[#LightBlue]  **Infraestructuras**
     *[#LightBlue]  **Procesos de negocio**
   *[#LightYellow]  **Social**
    *[#LightPink]  **Consultorìa**
     *[#LightBlue]  **Sentimental Analysis**
    *[#LightPink]  **Integraciòn**
     *[#LightBlue]  **Desarrollo de conectores**
      *_ Con 
       *[#LightBlue]  **Redes Sociales**
    *[#LightPink]  **Externalizaciòn**
     *[#LightBlue]  **Operaciòn Comunity Manager**
   *[#LightYellow]  **Movilidad**
    *[#LightGreen]  **Consultorìa**
     *[#LightBlue]  **Movilidad**
      *[#LightBlue]  **Definiciòn de pràctica**
       *_ de 
        *[#LightBlue]  **Gobierno TI**
    *[#LightGreen]  **Integraciòn**
     *[#LightBlue]  **Movilizaciòn**
      *_ de
       *[#LightBlue]  **Aplicaciones existentes**
     *[#LightBlue]  **Desarrollo y prueba**
      *_ de
       *[#LightBlue]  **APPS** 
     *[#LightBlue]  **Integraciòn de Soluciones**
      *[#LightBlue]  **Firma biomètrica**
      *[#LightBlue]  **SMS/mail certificados**
    *[#LightGreen]  **Externalizaciòn**
     *[#LightBlue]  **Operaciòn de plataformas**
      *_ de
       *[#LightBlue]  **Aplicaciones mòviles**
   *[#LightYellow]  **Analytics**
    *[#LightPink]  **Consultorìa**
     *[#LightBlue]  **Definiciòn**
      *_ de 
       *[#LightBlue]  **Big Data**
    *[#LightPink]  **Integraciòn**
     *[#LightBlue]  **Integraciòn de fuentes**
     *[#LightBlue]  **Desarrollo de CDM**
     *[#LightBlue]  **Integraciòn de**
      *[#LightBlue]  **Entornos relacionales**
      *[#LightBlue]  **Big Data**
    *[#LightPink]  **Externalizaciòn**
     *[#LightBlue]  **Factoria**
      *_ de 
       *[#LightBlue]  **Explotaciòn y mantenimiento**
        *_ de
         *[#LightBlue]  **Modelos analìticos**
   *[#LightYellow]  **Cloud**
    *[#LightGreen]  **Consultorìa**
     *[#LightBlue]  **Definiciòn**
      *_ de 
       *[#LightBlue]  **Estrategia Cloud**
       *[#LightBlue]  **Procedimientos de Operaciòn Cloud**
    *[#LightGreen]  **Integraciòn**
     *[#LightBlue]  **Desarrollo **
      *_ De
       *[#LightBlue]  **Herramientas de provisiòn CLOUD**
       *[#LightBlue]  **Aplicaciones CLOUD**
    *[#LightGreen]  **Externalizaciòn**
     *[#LightBlue]  **Operaciòn**
      *_ de
       *[#LightBlue]  **Servicios CLOUD**
 *[#LightGreen]  **¿Profesiòn de futuro?**
  *[#LightPink]  **Modo de vida**
   *_ que permite 
    *[#LightBlue]  **Alcanzar**
     *_ la
      *[#LightBlue]  **Felicidad Profesional**
 *[#LightGreen]  **Exigencias**
  *[#LightBlue]  **Actitud**
  *[#LightBlue]  **Aptitud**
  *[#LightBlue]  **Voluntad de mejora**
  *[#LightBlue]  **Compromiso**
 *[#LightGreen]  **Categorìas profesionales**
  *[#LightPink]  **Nivel**
   *[#LightBlue]  **Junior**
   *[#LightBlue]  **Senior**
   *[#LightBlue]  **Gerente**
   *[#LightBlue]  **Director**
    

@endmindmap
```mindmap
```

# Consultorìa de Software-Mapa conceptual
```plantuml
@startmindmap
*[#LightGoldenRodYellow]  **CONSULTORÌA ** \n **DE SOFTWARE**
 *[#LightPink]  **AXPE CONSULTING**
  *[#LightGreen]  **Desarrollo de Software**
   *[#LightYellow]  **Necesidad del cliente**
   *[#LightYellow]  **Procesos**
    *[#LightPink]  **Estudio de vialidad**
     *[#LightBlue]  **Estudiar**
      *_ lo que el
       *[#LightBlue]  **Cliente**
        *[#LightBlue]  **Quiere**  
    *[#LightPink]  **Diseño funcional**
     *[#LightBlue]  **Informaciòn**
      *_ De
       *[#LightBlue]  **Entrada**
       *[#LightBlue]  **Salida**
     *[#LightBlue]  **Modelo de datos**
     *[#LightBlue]  **Prototipo**
    *[#LightPink]  **Diseño tècnico**
     *_ La
      *[#LightBlue]  **Necesidad**
       *_ en un
        *[#LightBlue]  **Lenguaje de programaciòn**
     *_ Crear todos los
      *[#LightBlue]  **Procesos**
       *[#LightBlue]  **Prueba integrada**
     *_ Construir el
      *[#LightBlue]  **Software**
       *[#LightBlue]  **Pruebas**
    *[#LightPink]  **Pruebas de usuario**
  *[#LightGreen]  **Actividades desarrolladas**
   *[#LightYellow]  **Tareas**
    *_ Que permiten crear
     *[#LightBlue]  **Informatica**
      *_ de una
       *[#LightBlue]  **Empresa**
   *_ Dar
    *[#LightYellow]  **Servicios** 
     *_ A
      *[#LightBlue]  **Empresas**
   *[#LightYellow]  **Soluciones Software AD-HOC**
    *[#LightBlue]  **Natural / ADABAS**
    *[#LightBlue]  **J2EE**
    *[#LightBlue] **.net**
    *[#LightBlue]  **Sharepoint**
    *[#LightBlue]  **Com**
    *[#LightBlue]  **Heroku**
  *[#LightGreen]  **Tendencias**
   *_ en el
    *[#LightYellow]  **Desarrollo**
     *_ De
      *[#LightBlue]  **Servicios**
       *[#LightBlue]  **Cambio**
        *_ De
         *[#LightBlue]  **Infraestructura**
          *_ En una
           *[#LightBlue]  **Empresa**
       *[#LightBlue]  **Almacenar**
        *[#LightBlue]  **Informaciòn**
         *_ En la
          *[#LightBlue]  **Nube**
           *_ Ejemplo
            *[#LightBlue]  **Google**
       *[#LightBlue]  **Metodologìa**
        *_ De
         *[#LightBlue]  **Desarrollo**
          *_ De
           *[#LightBlue]  **Proyectos**
            *[#LightBlue]  **Ventajas**
             *[#LightBlue]  **Permiten**
              *_ Tener
               *[#LightBlue]  **Proyectos**
                *_ mas
                 *[#LightBlue]  **Pequeños**
            *[#LightBlue]  **Desventajas**
             *[#LightBlue]  **Perdida**
              *_ De
               *[#LightBlue]  **Documentaciòn**
  *[#LightGreen]  **Visita tècnica**
   *[#LightYellow]  **Aspectos**
    *_ Mas de
     *[#LightBlue]  **2000 profesionales**
      *[#LightBlue]  **Laborando**
@endmindmap
```mindmap
```
# APLICACIÒN DE LA INGENIERÌA DE SOFTWARE-MAPA CONCEPTUAL

```plantuml
@startmindmap
*[#LightGoldenRodYellow]  **APLICACIÒN DE LA** \n **INGENIERÌA DE** \n **SOFTWARE**
 *[#LightGreen]  **Stratesys**
  *_ Es una
   *[#LightPink]  **Empresa**
    *_ Requiere de
     *[#LightBlue]  **Conocimiento**
      *[#LightBlue]  **Ayuda a clientes**
     *[#LightBlue]  **Innovaciòn**
     *[#LightBlue]  **Especializaciòn**
     *[#LightBlue]  **Calidad**
    *[#LightBlue]  **Señas de identidades**
  *[#LightPink]  **Modelo de aplicaciòn**
   *[#LightYellow]  **Aplicaciòn pràctica**
   *[#LightYellow]  **Proyectos**
    *[#LightBlue]  **Fases**
     *[#LightBlue]  **Ànalisis**
     *[#LightBlue]  **Construcciòn**
     *[#LightBlue]  **Pruebas**
    *[#LightBlue]  **Tiempo**
     *[#LightBlue]  **Indefinido**
    *_ Que se cubren con
     *[#LightBlue]  **Programaciòn**
   *[#LightYellow]  **Procesos**
    *[#LightBlue]  **Enfoque mètodologico**
     *[#LightBlue]  **Desarrollo del proyecto**
    *[#LightBlue]  **Reuniòn de directores**
     *[#LightBlue]  **Visiòn global**
    *[#LightBlue]  **Nivel de detalle**
     *_ De cada
      *[#LightBlue]  **Proceso**
    *[#LightBlue]  **Requisito funcional**
     *[#LightBlue]  **Ànalisis funcional**
    *[#LightBlue]  **Aprobaciòn del cliente**
    *[#LightBlue]  **Anàlisis tècnico**
  *[#LightPink]  **Servicios de tecnologìa**
  *[#LightPink]  **Capital 100% español**
@endmindmap
```mindmap
```
